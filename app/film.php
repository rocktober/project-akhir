<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $table = "film";
    protected $fillable = ['judul', 'sinopsis', 'tahun',  'genre_id', 'poster',];
}
