@extends('template.master')

@section('title')
    Halaman Edit Buku
@endsection

@section('content')

<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="judul">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" id="nama" placeholder="Masukkan Genre">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection